package application

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class LoginLogout {
	@Keyword
	public Boolean Login(String url,String userName,String password){
		try{
			WebUI.openBrowser(url, FailureHandling.STOP_ON_FAILURE)
			//Login Page
			WebUI.delay(1)
			WebUI.maximizeWindow()
			WebUI.setViewPortSize(1920, 1080)
			WebUI.delay(2)
			WebUI.verifyElementPresent(findTestObject("Object Repository/LoginPage/userID"), 10, FailureHandling.STOP_ON_FAILURE)
			WebUI.setText(findTestObject("Object Repository/LoginPage/userID"), userName, FailureHandling.STOP_ON_FAILURE)
			WebUI.scrollToElement(findTestObject("Object Repository/LoginPage/password"), 10)
			WebUI.setText(findTestObject("Object Repository/LoginPage/password"), password, FailureHandling.STOP_ON_FAILURE)
			WebUI.scrollToElement(findTestObject("Object Repository/LoginPage/loginButton"), 10)
			WebUI.click(findTestObject("Object Repository/LoginPage/loginButton"), FailureHandling.STOP_ON_FAILURE)
			//Samsha
			WebUI.delay(2)
			if(WebUI.verifyElementPresent(findTestObject("Object Repository/LoginPage/samshaPopupConfirmation"), 3, FailureHandling.OPTIONAL)){
				WebUI.click(findTestObject("Object Repository/LoginPage/samshaPopupConfirmation"), FailureHandling.STOP_ON_FAILURE)
				WebUI.click(findTestObject("Object Repository/LoginPage/samshaPopupIAgreeButton"), FailureHandling.STOP_ON_FAILURE)

			}


			Boolean loginStatus=WebUI.verifyElementPresent(findTestObject("Object Repository/LoginPage/homePageVerification"), 20, FailureHandling.OPTIONAL)
			return loginStatus
		} catch(Exception e){
			return false
		}
	}

	@Keyword
	public Boolean Logout()
	{
		try{
			WebUI.click(findTestObject("Object Repository/LoginPage/LogoutButton"), FailureHandling.STOP_ON_FAILURE)
			WebUI.verifyElementPresent(findTestObject("Object Repository/LoginPage/LogoutConfirmationPopupYesButton"), 6, FailureHandling.STOP_ON_FAILURE)
			WebUI.click(findTestObject("Object Repository/LoginPage/LogoutConfirmationPopupYesButton"), FailureHandling.STOP_ON_FAILURE)
			Boolean LogoutStatus=WebUI.verifyElementPresent(findTestObject("Object Repository/LoginPage/userID"), 15, FailureHandling.STOP_ON_FAILURE)
			return LogoutStatus

		} catch(Exception e){
			return false
		}

	}
}
