package application

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper


import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class DemographicsApp {
	@Keyword
	public Boolean DemographcisAppNavigation(){
		try{
			WebUI.verifyElementPresent(findTestObject('Application/DemographicsApp/AppDemographicIcon'), 5, FailureHandling.STOP_ON_FAILURE)
			WebUI.click(findTestObject('Application/DemographicsApp/AppDemographicIcon'), FailureHandling.STOP_ON_FAILURE)
			Boolean appNavigationStatus=WebUI.verifyElementPresent(findTestObject('Application/DemographicsApp/SearchPatientPage/DA_SPP_searchButton'), 5, FailureHandling.OPTIONAL)
			return appNavigationStatus
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean searchPatientInDemographcisApp(String patientID, String patientLastName){
		try{
			//Search patient with PatientLastName and Patient ID
			WebUiCommonHelper.findWebElement(findTestObject('Application/DemographicsApp/SearchPatientPage/DA_SPP_PatientLastNameInputField'), 5).clear()
			WebUI.setText(findTestObject('Application/DemographicsApp/SearchPatientPage/DA_SPP_PatientLastNameInputField'), patientLastName, FailureHandling.STOP_ON_FAILURE)
			WebUiCommonHelper.findWebElement(findTestObject('Application/DemographicsApp/SearchPatientPage/DA_SPP_PatientIDinputField'), 5).clear()
			WebUI.delay(1)
			WebUI.setText(findTestObject('Application/DemographicsApp/SearchPatientPage/DA_SPP_PatientIDinputField'), patientID, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(1)
			WebUI.click(findTestObject('Application/DemographicsApp/SearchPatientPage/DA_SPP_searchButton'), FailureHandling.STOP_ON_FAILURE)
			TestObject patientSearchObject = findTestObject('Base/commanXpath')
			patientSearchObject.findProperty('xpath').setValue("//div[text()='" + patientID+"']/following::td/div[text()='"+patientLastName+"']")
			Boolean patietSearchStatus=WebUI.verifyElementPresent(patientSearchObject, 15, FailureHandling.OPTIONAL)
			WebUI.delay(3)
			WebUI.click(patientSearchObject, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(3)
			return patietSearchStatus
		} catch(Exception e){
			return false
		}
	}
}
