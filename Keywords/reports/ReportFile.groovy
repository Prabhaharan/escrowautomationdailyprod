package reports

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable
import junit.framework.TestListener
import sun.security.ssl.Alerts
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI
import com.relevantcodes.extentreports.DisplayOrder
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.Alert
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot
import org.openqa.selenium.WebDriver

import org.apache.commons.io.FileUtils
import java.awt.Rectangle
import java.awt.Robot
import java.awt.image.BufferedImage
import java.io.IOException
import java.text.SimpleDateFormat
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.jsoup.nodes.Document
import org.jsoup.select.Elements
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.interactions.Actions as Actions
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.awt.Toolkit
import javax.imageio.ImageIO;
import keywordsLibrary.CommomLibrary


public class ReportFile {
	private static ExtentReports extent;
	@Keyword
	public String propertyFileForReport(String reportFolder){
		try{
			File dir = new File(".\\configuration\\"+reportFolder)
			if(!dir.isDirectory()){
				dir.mkdir()
			}
			File configFile= new File(".\\configuration\\"+reportFolder+"\\reportConfig.txt")
			if(configFile.exists() && configFile.isDirectory()) {
				configFile.delete()
			}
			configFile.createNewFile()
			Date date=new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss aa z");
			String fileName=dateFormat.format(date).toString()
			println fileName
			BufferedWriter writer = new BufferedWriter(new FileWriter(".\\configuration\\"+reportFolder+"\\reportConfig.txt"));
			writer.write(fileName);
			writer.close()
			return "Success"
		}catch(Exception e){
			return "Fail"+e
		}
	}
	@Keyword
	public static ExtentReports getInstance(String reportFolder){
		int extendCount=0
		if (extent == null) {
			FileReader fr =new FileReader(".\\configuration\\"+reportFolder+"\\reportConfig.txt");
			String folderPath=fr.text
			folderPath=folderPath.replace(":", "_").replace(" ", "_")
			fr.close()
			Date d=new Date();
			String fileName=d.toString().replace(":", "_").replace(" ", "_")+"-"+CommomLibrary.getRandomNumber()+".html";
			extent = new ExtentReports(System.getProperty("user.dir")+"\\CustomizedReports\\"+reportFolder+"\\"+folderPath+"\\TestSuites\\"+fileName, true, DisplayOrder.NEWEST_FIRST);
			extent.loadConfig(new File(System.getProperty("user.dir")+"\\configuration\\"+reportFolder+"//ReportsConfig.xml"));
			extent.addSystemInfo("Selenium Version", "2.53.0").addSystemInfo(
					"Environment", "QA");
		}

		return extent;
	}
	@Keyword
	public String print(ExtentTest test,boolean verifyStatus,String printStatement) {
		if(verifyStatus){
			test.log(LogStatus.INFO, printStatement);
			return "Pass"
		}else{
			test.log(LogStatus.FAIL, "Fail : "+printStatement);
			takeScreenshot(test)
			return "Fail"
		}
	}
	@Keyword
	public String takeScreenshot(ExtentTest test) {
		WebDriver driver = DriverFactory.getWebDriver()
		Date d = new Date();
		String screenshotFile = d.toString().replace(":", "_").replace(" ", "_")+"-" +CommomLibrary.getRandomNumber()+ ".png";
		try{
			WebUI.takeScreenshot(System.getProperty("user.dir") + "//screenshots//" + screenshotFile)
			test.log(LogStatus.INFO, "Screenshot-> "
					+ test.addScreenCapture(System.getProperty("user.dir") + "//screenshots//" + screenshotFile));
		}catch(Exception e){
			test.log(LogStatus.INFO, "**Unexpected Alert Pop is opened - Cannot take screenshot ");
		}
	}
}
