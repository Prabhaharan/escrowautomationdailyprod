import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile



import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.annotation.BeforeTestSuite
import com.kms.katalon.core.annotation.AfterTestCase
import com.kms.katalon.core.annotation.AfterTestSuite
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.context.TestSuiteContext
import org.apache.commons.io.FileUtils
import java.io.IOException
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.jsoup.nodes.Document
import org.jsoup.select.Elements
import org.testng.SkipException

class NewTestListener {
	/**
	 * Executes after every test suite ends.
	 * @param testSuiteContext: related information of the executed test suite.
	 */
	@AfterTestCase
	def sampleAfterTestCase(TestCaseContext TestCaseContext){
		println TestCaseContext.testCaseId
		if((!(TestCaseContext.testCaseId).equals("Test Cases/Base/ReportConfig")) ||(!(TestCaseContext.testCaseId).equals("Test Cases/FunctionalLibrary/login")) ||(!(TestCaseContext.testCaseId).equals("Test Cases/FunctionalLibrary/Logout"))){	
		String reportFolder=TestCaseContext.testCaseVariables.get("reportFolder")
		println reportFolder
		FileReader fr =new FileReader(".\\configuration\\"+reportFolder+"\\reportConfig.txt");
		String folderPath=fr.text
		folderPath=folderPath.replace(":", "_").replace(" ", "_");
		fr.close()
		File folder = new File(System.getProperty("user.dir")+"\\CustomizedReports\\"+reportFolder+"\\"+folderPath+"\\TestSuites\\");
		File[] listOfFiles = folder.listFiles();
		File testSuiteReports=new File(System.getProperty("user.dir")+"\\CustomizedReports\\"+reportFolder+"\\"+folderPath+"\\TestSuites\\"+listOfFiles[0].getName())
		File testSuiteCollectionReports=new File(System.getProperty("user.dir")+"\\CustomizedReports\\"+reportFolder+"\\"+folderPath+"\\FinalReport.html")
		FileUtils.copyFile(testSuiteReports, testSuiteCollectionReports)	
		Document finalReportDoc = Jsoup.parse(testSuiteCollectionReports, null)
		Elements finalReportRemoveContent = finalReportDoc.select("ul#test-collection").select("li");
		finalReportRemoveContent.remove()
		Elements finalReportAddContent = finalReportDoc.select("ul#test-collection");
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
			  System.out.println("File " + listOfFiles[i].getName());
			  Document testSuitesDoc = Jsoup.parse(listOfFiles[i], null)
			  Elements child=testSuitesDoc.select("ul#test-collection").select("li")

			  for(Element parent:finalReportAddContent){
				  for(Element testCaseResult :child){
					  parent.appendChild(testCaseResult)
				  }
			  }
			} else if (listOfFiles[i].isDirectory()) {
			  System.out.println("Directory " + listOfFiles[i].getName());
			}
		  }
		PrintWriter writer = new PrintWriter(testSuiteCollectionReports,"UTF-8");
		writer.write(finalReportDoc.html());
		writer.flush();
		writer.close();	
		}
	}
//	@AfterTestSuite
//	def sampleAfterTestSuite(TestSuiteContext testSuiteContext) {
//		FileReader fr =new FileReader(".\\configuration\\reportConfig.txt");
//		String folderPath=fr.text
//		folderPath=folderPath.replace(":", "_").replace(" ", "_");
//		fr.close()
//		File folder = new File(System.getProperty("user.dir")+"\\CustomizedReports\\"+folderPath+"\\TestSuites\\");
//		File[] listOfFiles = folder.listFiles();
//		File testSuiteReports=new File(System.getProperty("user.dir")+"\\CustomizedReports\\"+folderPath+"\\TestSuites\\"+listOfFiles[0].getName())
//		File testSuiteCollectionReports=new File(System.getProperty("user.dir")+"\\CustomizedReports\\"+folderPath+"\\FinalReport.html")
//		FileUtils.copyFile(testSuiteReports, testSuiteCollectionReports)	
//		Document finalReportDoc = Jsoup.parse(testSuiteCollectionReports, null)
//		Elements finalReportRemoveContent = finalReportDoc.select("ul#test-collection").select("li");
//		finalReportRemoveContent.remove()
//		Elements finalReportAddContent = finalReportDoc.select("ul#test-collection");
//		listOfFiles[1]
//		for (int i = 0; i < listOfFiles.length; i++) {
//			if (listOfFiles[i].isFile()) {
//			  System.out.println("File " + listOfFiles[i].getName());
//			  Document testSuitesDoc = Jsoup.parse(listOfFiles[i], null)
//			  Elements child=testSuitesDoc.select("ul#test-collection").select("li")
//
//			  for(Element parent:finalReportAddContent){
//				  for(Element testCaseResult :child){
//					  parent.appendChild(testCaseResult)
//				  }
//			  }
//			} else if (listOfFiles[i].isDirectory()) {
//			  System.out.println("Directory " + listOfFiles[i].getName());
//			}
//		  }
//		PrintWriter writer = new PrintWriter(testSuiteCollectionReports,"UTF-8");
//		writer.write(finalReportDoc.html());
//		writer.flush();
//		writer.close();	
//	}
}