<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_PW_VerticalElipsisIcon</name>
   <tag></tag>
   <elementGuidId>d0a275a2-569e-4d0a-bfb0-48565ca1aa18</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//gsi-program-table//md-icon[@md-svg-icon='dots-vertical']</value>
   </webElementProperties>
</WebElementEntity>
