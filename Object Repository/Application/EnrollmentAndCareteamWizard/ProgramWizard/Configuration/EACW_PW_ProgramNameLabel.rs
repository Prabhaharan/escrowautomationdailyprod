<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_PW_ProgramNameLabel</name>
   <tag></tag>
   <elementGuidId>dc8cb4e0-b780-4f9a-b4d5-4c13111e2877</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button/span[contains(text(),&quot;Program Name&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
