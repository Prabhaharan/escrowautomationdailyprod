<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_PW_CRISP_ParentName</name>
   <tag></tag>
   <elementGuidId>ba592f67-4594-4f84-9225-b5cdebe30ed1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//md-option/div[normalize-space(text()) = 'Care Coordination Plus']</value>
   </webElementProperties>
</WebElementEntity>
