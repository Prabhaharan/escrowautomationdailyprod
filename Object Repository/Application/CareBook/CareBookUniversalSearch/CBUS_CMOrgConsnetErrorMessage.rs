<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CBUS_CMOrgConsnetErrorMessage</name>
   <tag></tag>
   <elementGuidId>6fe0c320-2ce3-4ed9-a36f-43a4029437fc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@aria-label='Your organization has not been granted access to this patient.']</value>
   </webElementProperties>
</WebElementEntity>
