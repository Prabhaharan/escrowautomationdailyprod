<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CBUS_SearchButtonEnable</name>
   <tag></tag>
   <elementGuidId>a1dde72f-8a2a-4d54-8352-0767bb62501d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@type=&quot;submit&quot;]/..//span[contains(text(),'Search')]</value>
   </webElementProperties>
</WebElementEntity>
