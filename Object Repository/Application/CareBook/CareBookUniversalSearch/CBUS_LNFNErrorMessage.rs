<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CBUS_LNFNErrorMessage</name>
   <tag></tag>
   <elementGuidId>81baf47c-ed94-47b0-9e59-dd691f7321fd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(text(),'The format is &quot;Last Name, First Name&quot;')]</value>
   </webElementProperties>
</WebElementEntity>
