<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ConsentPopupMinorBar</name>
   <tag></tag>
   <elementGuidId>876b5c45-eb82-45ae-ab97-b8270728c664</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@aria-label='Information Sharing']//div[@class='md-thumb-container']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@aria-label='Information Sharing']//div[@class='md-thumb-container']/div</value>
   </webElementProperties>
</WebElementEntity>
