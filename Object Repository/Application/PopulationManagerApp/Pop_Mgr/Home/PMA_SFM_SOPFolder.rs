<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PMA_SFM_SOPFolder</name>
   <tag></tag>
   <elementGuidId>d43b7f8a-94bf-483f-a8f8-0995ed51e0d6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@path=&quot;/public/Standard Operational Reports&quot;][count(. | //*[@ref_element = 'Object Repository/Applications/PopulationManagerApp/Pop_Mgr/Home/iframe_browser.perspective']) = count(//*[@ref_element = 'Object Repository/Applications/PopulationManagerApp/Pop_Mgr/Home/iframe_browser.perspective'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@path=&quot;/public/Standard Operational Reports&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath1</name>
      <type>Main</type>
      <value>//div/div[text()='Standard Operational Reports'][3]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Application/PopulationManagerApp/Pop_Mgr/Home/iframe_browser.perspective</value>
   </webElementProperties>
</WebElementEntity>
