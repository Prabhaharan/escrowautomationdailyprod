<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PMA_SFM_HomeButton</name>
   <tag></tag>
   <elementGuidId>c2f7802a-a9ad-4543-93d6-35fffc8cb05b</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//tr/td[@class='custom-dropdown-arrow custom-dropdown-arrow-major']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Application/PopulationManagerApp/Pop_Mgr/Home/iframe_browser.perspective</value>
   </webElementProperties>
</WebElementEntity>
