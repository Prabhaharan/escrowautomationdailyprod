<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_gwt-Image pentaho-tabWidge</name>
   <tag></tag>
   <elementGuidId>a4b686b1-6d01-41e0-bb95-4f88e692a587</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;solutionNavigatorAndContentPanel&quot;)/div[@class=&quot;alignleft&quot;]/table[@class=&quot;pentaho-tab-panel&quot;]/tbody[1]/tr[1]/td[1]/div[@class=&quot;pentaho-tab-bar&quot;]/div[@class=&quot;pentaho-tabWidget pentaho-tabWidget-selected&quot;]/table[1]/tbody[1]/tr[1]/td[2]/img[@class=&quot;gwt-Image pentaho-tabWidget-close pentaho-closebutton pentaho-imagebutton-hover&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://qa.gsihealth.net/pentaho/content/common-ui/resources/themes/images/spacer.gif</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>gwt-Image pentaho-tabWidget-close pentaho-closebutton pentaho-imagebutton-hover</value>
   </webElementProperties>
</WebElementEntity>
