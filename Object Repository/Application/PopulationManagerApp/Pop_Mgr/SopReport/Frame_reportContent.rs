<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Frame_reportContent</name>
   <tag></tag>
   <elementGuidId>3ffb92bc-e72f-4339-a4e8-8f48e18ef873</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>iframe</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;reportContent&quot;)</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://qa.gsihealth.net/pentaho/api/repos/%3Apublic%3AStandard%20Operational%20Reports%3AAssessmentReport.prpt/report?ts=1503922956401&amp;Enrollment_status=ALL&amp;Assessment_Status=ALL&amp;AssessmentName=ALL&amp;org_name=ALL&amp;program_name=ALL&amp;program_status=ALL&amp;FromDate=2017-07-28T00%3A00%3A00.000-0400&amp;ToDate=2017-08-28T00%3A00%3A00.000-0400&amp;output-target=table%2Fhtml%3Bpage-mode%3Dstream&amp;COMMUNITY_ID=102&amp;accepted-page=-1&amp;showParameters=true&amp;renderMode=REPORT&amp;htmlProportionalWidth=false</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>unselectable</name>
      <type>Main</type>
      <value>on</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>frameborder</name>
      <type>Main</type>
      <value>0</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>reportContent</value>
   </webElementProperties>
</WebElementEntity>
