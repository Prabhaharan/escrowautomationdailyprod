<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ParentProgram</name>
   <tag></tag>
   <elementGuidId>35279e00-b2e6-465a-80b4-14d3c1dffe02</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[text()='Parent Program']/following::select[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Parent Program']/following::select[1]</value>
   </webElementProperties>
</WebElementEntity>
