<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AssessmentName-all</name>
   <tag></tag>
   <elementGuidId>89d570f6-0099-45a0-a735-738e7af34bfc</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Assessment Name']/following::select[1]/option[@value='ALL']</value>
   </webElementProperties>
</WebElementEntity>
