<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Orgname-all</name>
   <tag></tag>
   <elementGuidId>32e91ca5-30e6-46ec-a732-7e3c0740ad39</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Organization Name']/following::select[1]/option[@value='ALL']</value>
   </webElementProperties>
</WebElementEntity>
