<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CareTeam</name>
   <tag></tag>
   <elementGuidId>141d3745-0bd0-4d2c-bf48-1d51ae8fbb6c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[text()='Care Team Role']/following::select[1]/option[@value='Administrator']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Care Team Role']/following::select[1]/option[@value='Administrator']</value>
   </webElementProperties>
</WebElementEntity>
