<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AssesmentStatus-AL</name>
   <tag></tag>
   <elementGuidId>19285d89-1dd7-4fa5-aa25-a08c8b06b134</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Assessment Status']/following::select[1]/option[@value='ALL']</value>
   </webElementProperties>
</WebElementEntity>
