<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AdmitDate</name>
   <tag></tag>
   <elementGuidId>99fa31e6-4c4e-4982-9daa-54ea7cd045e1</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='From Admit Date']/following::input[3]</value>
   </webElementProperties>
</WebElementEntity>
