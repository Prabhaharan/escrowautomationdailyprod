<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AssessmentStatus_dropdown</name>
   <tag></tag>
   <elementGuidId>83b5eb77-583b-439c-bada-43c5d92122e9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Assessment Status']/following::select[1]</value>
   </webElementProperties>
</WebElementEntity>
