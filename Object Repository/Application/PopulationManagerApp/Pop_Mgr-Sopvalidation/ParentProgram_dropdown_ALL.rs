<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ParentProgram_dropdown_ALL</name>
   <tag></tag>
   <elementGuidId>8ff4cb55-6665-4f9f-abde-9649d37d33df</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[text()='Parent Program']/following::select[1]/option[@value='-3']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Parent Program']/following::select[1]/option[@value='-3']</value>
   </webElementProperties>
</WebElementEntity>
