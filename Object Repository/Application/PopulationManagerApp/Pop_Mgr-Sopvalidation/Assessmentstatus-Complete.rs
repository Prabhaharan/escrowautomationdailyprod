<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Assessmentstatus-Complete</name>
   <tag></tag>
   <elementGuidId>b202e205-d513-4a0f-ae14-db06dc6af042</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Assessment Status']/following::select[1]/option[@value='COMPLETE']</value>
   </webElementProperties>
</WebElementEntity>
