<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ParentProgram_dropdown</name>
   <tag></tag>
   <elementGuidId>e8719838-cd30-4aa7-9905-23b75f7e8ab1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Parent Program']/following::select[1]</value>
   </webElementProperties>
</WebElementEntity>
