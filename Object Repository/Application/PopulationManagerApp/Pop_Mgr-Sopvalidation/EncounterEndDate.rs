<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EncounterEndDate</name>
   <tag></tag>
   <elementGuidId>987ba91f-660b-4977-955b-1912e1eac3ae</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Encounter End Date']/following::input[3]</value>
   </webElementProperties>
</WebElementEntity>
