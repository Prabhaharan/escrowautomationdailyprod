<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ParentName_dropdown</name>
   <tag></tag>
   <elementGuidId>689f9a12-0560-45e5-abde-8012a1deaa49</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Parent Name']/following::select[1]</value>
   </webElementProperties>
</WebElementEntity>
