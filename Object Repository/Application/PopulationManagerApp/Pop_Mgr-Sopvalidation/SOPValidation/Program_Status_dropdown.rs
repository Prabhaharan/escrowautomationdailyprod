<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Program_Status_dropdown</name>
   <tag></tag>
   <elementGuidId>c45a5f98-7447-411b-ad55-69019ce18dc7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Program Status']/following::select[1]</value>
   </webElementProperties>
</WebElementEntity>
