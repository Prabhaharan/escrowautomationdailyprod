<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DA_SPP_AddPatientButtton</name>
   <tag></tag>
   <elementGuidId>06efa56a-1d27-45b7-97ee-dd4ca5354d19</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td[contains(text(),&quot;Add Patient&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
