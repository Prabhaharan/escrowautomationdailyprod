<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DA_VPP_addPatientToPatientPanelSuccessfullOkButton</name>
   <tag></tag>
   <elementGuidId>b7308a27-e3c8-40ce-8fdc-464fa7d610ce</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[contains(text(),'Patient added to your patient panel.')]/../following::*/*[contains(text(),'OK')]</value>
   </webElementProperties>
</WebElementEntity>
