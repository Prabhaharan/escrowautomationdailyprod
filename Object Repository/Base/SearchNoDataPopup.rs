<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>SearchNoDataPopup</name>
   <tag></tag>
   <elementGuidId>44bc4ab0-90de-4aba-b07d-55f049308e87</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td[contains(text(),'Search did not return any results. Please modify your search criteria.')]</value>
   </webElementProperties>
</WebElementEntity>
