import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


def ExtentReports rep = CustomKeywords.'reports.ReportFile.getInstance'()
def ExtentTest test
scriptStatus=false
int stepCount=1
def TestCase= env+"-DemographicsUpdateVerification" /**Mention the Test Case Name Here=> ENV + TestCaseName**/
def printStatemnet=""
test = rep.startTest(TestCase)
verifyStatus=false
try{
          /*****************Test Script Start from Here************
           * 
           * Following three lines should follow
           * for each steps to reproduce statement
           * 
           */
verifyStatus=/*********Write the Verification Statement / Method for executing the Test Step***********/
printStatemnet="Step "+(stepCount++)+"Mention the Steps Name Here"
WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test,verifyStatus,printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE) /**Don't change this line**/
		/****
		 * 
		 * Continue the script
		 * and follow the above 3 lines when the Steps statement needed
		 */

/***At the end of each Test case we should mention the scrpt status as success***/
scriptStatus=true
/********Keep below mentioned Catch and Finally statement for each test cases : *********/
}catch(Exception e){
if(verifyStatus){
	printStatemnet=" Exception occured when processing the Next Step : "+e
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test,false,printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
}
}finally{
		if(scriptStatus){
			test.log(LogStatus.PASS, TestCase);
		}else{
			test.log(LogStatus.FAIL, TestCase);
			WebUI.verifyMatch("PASS", "FAIL", false, FailureHandling.STOP_ON_FAILURE)
		}
		rep.endTest(test);
		rep.flush();
}
