import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.main.CustomKeywordDelegatingMetaClass
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import junit.framework.TestListener as TestListener
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.relevantcodes.extentreports.ExtentReports as ExtentReports
import com.relevantcodes.extentreports.ExtentTest as ExtentTest
import com.relevantcodes.extentreports.LogStatus as LogStatus

ExtentReports rep = CustomKeywords.'reports.ReportFile.getInstance'(reportFolder)

ExtentTest test

println(test)

scriptStatus = false

int stepCount = 1

def TestCase = ENV + '- Login into dashboard'

test = rep.startTest(TestCase)

println(test)

verifyStatus = false

def printStatement = ''

//Open Browser
try{
	//Login to the Dashboard
	printStatement="Step "+(stepCount++)+": Login into the Dashboard"
	verifyStatus=CustomKeywords.'application.LoginLogout.Login'(url,userName,password)
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatement), "Pass", false, FailureHandling.STOP_ON_FAILURE)
	
	printStatement="Step "+(stepCount++)+": Logout from Dashboard"
	verifyStatus=CustomKeywords.'application.LoginLogout.Logout'()
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatement), "Pass", false, FailureHandling.STOP_ON_FAILURE)
	scriptStatus = true
}catch (Exception e) {
    if (verifyStatus) {
        printStatement = ((printStatement + ' ->**** Exception occured***: ') + e)

        CustomKeywords.'reports.ReportFile.takeScreenshot'(test)
    }
} 
finally { 
    if (scriptStatus) {
		println('Pass')
        test.log(LogStatus.PASS, TestCase)
        rep.endTest(test)
        rep.flush()
		WebUI.closeBrowser()
    } else {
	 println('Fail')
	
        test.log(LogStatus.FAIL, TestCase)
        rep.endTest(test)
        rep.flush()
		WebUI.closeBrowser()
        WebUI.verifyMatch("Config Verification", 'Config Verification Failed', false, FailureHandling.STOP_ON_FAILURE)
    }
}