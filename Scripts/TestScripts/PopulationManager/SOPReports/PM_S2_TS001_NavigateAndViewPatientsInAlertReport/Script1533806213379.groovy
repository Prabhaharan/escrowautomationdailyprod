import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.apache.commons.lang.StringUtils;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



//def fromDate=TestDataFactory.findTestData("PopulationManager/CommonParameter").getValue("fromDate", 1)
//FromDate-Test Data

ExtentReports rep = CustomKeywords.'reports.ReportFile.getInstance'(reportFolder)
ExtentTest test
println test
scriptStatus=false
int stepCount=1
def TestCase=ENV+"_PM_S2_TS001_NavigateAndViewPatientsInAlertsReport"
test = rep.startTest(TestCase)
println test
verifyStatus=false
def printStatemnet=""

try{

    //Login to the Dashboard
	printStatement="Step "+(stepCount++)+": Login into the Dashboard"
	verifyStatus=CustomKeywords.'application.LoginLogout.Login'(url,userName,password)
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatement), "Pass", false, FailureHandling.STOP_ON_FAILURE)
    WebUI.delay(3)
	//Click and Navigate to Population Manager App
	printStatement="Step "+(stepCount++)+": Click and Navigate to Population Manager App"
	verifyStatus=CustomKeywords.'application.PopulationManagerIcon.PopulationManagerAppNavigation'()
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatement), "Pass", false, FailureHandling.STOP_ON_FAILURE)
	WebUI.delay(3)
    //Navigate to SOP folder
    printStatement="Step "+(stepCount++)+": Navigate to SOP folder"
	verifyStatus=CustomKeywords.'application.PopulationManagerIcon.StandardOperationReportNavigation'()
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatement), "Pass", false, FailureHandling.STOP_ON_FAILURE)
    WebUI.delay(3)
	// Navigate to Alerts Report
    WebUI.doubleClick(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/AlertReport'), FailureHandling.STOP_ON_FAILURE)
    printStatemnet="Step "+(stepCount++)+": Navigate to Alerts Report"
    verifyStatus=WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30, FailureHandling.STOP_ON_FAILURE)
    WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
    WebUI.switchToFrame(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30)
    WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/FromDate'),50, FailureHandling.STOP_ON_FAILURE)
    WebUI.delay(3)
	// Select Parameter Values
printStatemnet="Step "+(stepCount++)+": Select Parameter Values"
verifyStatus=WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/FromDate'), 30, FailureHandling.STOP_ON_FAILURE)
WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
TestObject parentProgramSelectXpath = findTestObject('Object Repository/Base/commanAlertXpath')
parentProgramSelectXpath.findProperty('xpath').setValue("//select/option[text()='"+ParentProgram+"']")
WebUI.scrollToElement(parentProgramSelectXpath, 10, FailureHandling.STOP_ON_FAILURE)
WebUI.doubleClick(parentProgramSelectXpath)
WebUI.delay(3)
TestObject programNameSelectXpath = findTestObject('Object Repository/Base/commanAlertXpath')
programNameSelectXpath.findProperty('xpath').setValue("//select/option[text()='"+ProgramName+"']")
WebUI.scrollToElement(programNameSelectXpath, 10, FailureHandling.STOP_ON_FAILURE)
WebUI.doubleClick(programNameSelectXpath)
WebUI.delay(3)
TestObject programStatusSelectXpath = findTestObject('Object Repository/Base/commanAlertXpath')
programStatusSelectXpath.findProperty('xpath').setValue("//select/option[text()='"+ProgramStatus+"']")
WebUI.scrollToElement(programStatusSelectXpath, 10, FailureHandling.STOP_ON_FAILURE)
WebUI.doubleClick(programStatusSelectXpath)
WebUI.delay(3)
TestObject organizationSelectXpath = findTestObject('Object Repository/Base/commanAlertXpath')
organizationSelectXpath.findProperty('xpath').setValue("//select/option[text()='"+Organization+"']")
WebUI.scrollToElement(organizationSelectXpath, 5, FailureHandling.STOP_ON_FAILURE)
WebUI.doubleClick(organizationSelectXpath)
WebUI.delay(3)
TestObject alertNameSelectXpath = findTestObject('Object Repository/Base/commanAlertXpath')
alertNameSelectXpath.findProperty('xpath').setValue("//select/option[text()='"+AlertName+"']")
WebUI.scrollToElement(alertNameSelectXpath, 5, FailureHandling.STOP_ON_FAILURE)
WebUI.doubleClick(alertNameSelectXpath)
WebUI.delay(3)
TestObject severitySelectXpath = findTestObject('Object Repository/Base/commanAlertXpath')
severitySelectXpath.findProperty('xpath').setValue("//select/option[text()='"+Severity+"']")
WebUI.scrollToElement(severitySelectXpath, 5, FailureHandling.STOP_ON_FAILURE)
WebUI.doubleClick(severitySelectXpath)
WebUI.delay(3)
WebUI.setText(findTestObject('Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/FromDate'), FromDate)
WebUI.delay(3)

WebUI.setText(findTestObject('Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/ToDate'), ToDate)
WebUI.delay(3)
WebUI.setText(findTestObject('Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/PatientID'), PatientID)
//Click on the View Reports button
WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/View Report '), 30, FailureHandling.STOP_ON_FAILURE)
WebUI.doubleClick(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/View Report '))
WebUI.delay(3)
WebUI.switchToFrame(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_reportContent'), 30)
WebUI.verifyElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Patient ID'), 120, FailureHandling.STOP_ON_FAILURE)
printStatemnet="Step "+(stepCount++)+": Verify patientId details display in the view report"
String a = WebUI.getText(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Patient ID'), FailureHandling.STOP_ON_FAILURE)
println "Patient from the view Report"+a
verifyStatus = StringUtils.isNumeric(a);
WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test,verifyStatus,printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
WebUI.closeWindowIndex(1)
WebUI.switchToDefaultContent()
//Logout the Dashboard
printStatement="Step "+(stepCount++)+": Logout from Dashboard"
	verifyStatus=CustomKeywords.'application.LoginLogout.Logout'()
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatement), "Pass", false, FailureHandling.STOP_ON_FAILURE)
scriptStatus=true
}catch(Exception e){
if(verifyStatus){
printStatemnet=printStatemnet+" ->**** Exception occured***: "+e
CustomKeywords.'reports.ReportFile.takeScreenshot'(test)
}
}finally{
	if(scriptStatus){
		test.log(LogStatus.PASS, TestCase);
		rep.endTest(test);
		rep.flush();
	}else{
		test.log(LogStatus.FAIL, TestCase);
		rep.endTest(test);
		rep.flush();
		WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test,false,printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
	}
	
}

