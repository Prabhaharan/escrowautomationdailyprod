import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.keyword.builtin.VerifyMatchKeyword
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import junit.framework.TestListener

import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

ExtentReports rep = CustomKeywords.'reports.ReportFile.getInstance'(reportFolder)
ExtentTest test
println test
scriptStatus=false
int stepCount=1
def TestCase=ENV+"-MS_S1_TS003_Reply and Receive Mail"
test = rep.startTest(TestCase)
println test
verifyStatus=false
def printStatemnet=""
try{
	//Test Data
	def mailSubject = CustomKeywords.'keywordsLibrary.CommomLibrary.randomText'()+CustomKeywords.'keywordsLibrary.CommomLibrary.getRandomNumber'()
//Login to the Sender Dashboard
	printStatemnet="Step "+(stepCount++)+": Login into the Sender's Dashboard"
	verifyStatus=CustomKeywords.'application.LoginLogout.Login'(url,userName,password)
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
//Navigate to the Sender Messages App
	printStatemnet="Step "+(stepCount++)+": Navigate to Sender's MessagesApp"
	verifyStatus= CustomKeywords.'application.MessagesApp.MessagesAppNavigation'()
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
//Send New Mail
	printStatemnet="Step "+(stepCount++)+": Send new mail to the receiver end"
	WebUI.click(findTestObject('Application/Messages/MS_HomePage/MS_HP_NewButton'),FailureHandling.STOP_ON_FAILURE)
//Enter To,Cc,Bcc mail address
	WebUI.clearText(findTestObject('Application/Messages/MS_HomePage/MS_HP_ToEmailAddressInput'))
	WebUI.setText(findTestObject('Application/Messages/MS_HomePage/MS_HP_ToEmailAddressInput'),ToMailID)
//Enter Subject
	WebUI.setText(findTestObject('Application/Messages/MS_HomePage/MS_HP_Subject'),mailSubject,FailureHandling.STOP_ON_FAILURE)
	WebUI.delay(1)
//click send button
	WebUI.click(findTestObject('Application/Messages/MS_HomePage/MS_HP_Sendbutton'),FailureHandling.STOP_ON_FAILURE)
	WebUI.click(findTestObject('Application/Messages/MS_HomePage/MS_HP_RefreshButton'),FailureHandling.STOP_ON_FAILURE)
	WebUI.delay(4)
//Verify mail in Sender's Sent Items
	printStatemnet="Step "+(stepCount++)+": Verify sent mail in sender's sent items folder"
	WebUI.click(findTestObject('Application/Messages/MS_HomePage/MS_HP_FolderSent'),FailureHandling.STOP_ON_FAILURE)
	WebUI.verifyElementPresent(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'("//td[text()='"+mailSubject+"']"), 10, FailureHandling.OPTIONAL)
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
//Log out Senders Message App window
	WebUI.click(findTestObject('Application/Messages/MS_HomePage/MS_HP_LogoutMessageappWindow'),FailureHandling.STOP_ON_FAILURE)
	WebUI.delay(5)
	WebUI.switchToWindowIndex(0)
//Log out from Sender
	printStatemnet="Step "+(stepCount++)+": Logout the Dashboard from sender"
	verifyStatus=CustomKeywords.'application.LoginLogout.Logout'()
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
//Login to the  Dashboard as Receiver(To address)
	printStatemnet="Step "+(stepCount++)+": Login into the Dashboard as Receiver(To address)"
	verifyStatus=CustomKeywords.'application.LoginLogout.Login'(url,TouserID,password)
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
//Navigate to the Receiver(TO) Messages App
   printStatemnet="Step "+(stepCount++)+": Navigate to Receiver(To address) MessagesApp"
	verifyStatus= CustomKeywords.'application.MessagesApp.MessagesAppNavigation'()
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
//Verify receiver(TO) Inbox and Log out from messages app window
	printStatemnet="Step "+(stepCount++)+":Verify Messages In Receiver(To address) Inbox folder"
	verifyStatus= CustomKeywords.'application.MessagesApp.VerifyInboxEmail'(mailSubject)
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
//Reply Message to Sender
	printStatemnet="Step "+(stepCount++)+":Reply message from Receiver to Sender"
	WebUI.click(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'("//td[text()='"+mailSubject+"']"),FailureHandling.STOP_ON_FAILURE)	
	WebUI.click(findTestObject('Application/Messages/MS_HomePage/MS_HP_ReplyButton'),FailureHandling.STOP_ON_FAILURE)
	WebUI.click(findTestObject('Application/Messages/MS_HomePage/MS_HP_Sendbutton'),FailureHandling.STOP_ON_FAILURE)
	WebUI.click(findTestObject('Application/Messages/MS_HomePage/MS_HP_Sendbutton'),FailureHandling.STOP_ON_FAILURE)
	WebUI.click(findTestObject('Application/Messages/MS_HomePage/MS_HP_RefreshButton'),FailureHandling.STOP_ON_FAILURE)
	WebUI.delay(4)
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
//Verify mail in Reply sender Sent Items
	printStatemnet="Step "+(stepCount++)+": Verify sent mail in Reply sender's sent items folder"
	WebUI.click(findTestObject('Application/Messages/MS_HomePage/MS_HP_FolderSent'),FailureHandling.STOP_ON_FAILURE)
	WebUI.verifyElementPresent(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'("//td[text()='"+"Re: "+mailSubject+"']"), 10, FailureHandling.OPTIONAL)
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
	WebUI.click(findTestObject('Application/Messages/MS_HomePage/MS_HP_LogoutMessageappWindow'),FailureHandling.STOP_ON_FAILURE)
	WebUI.delay(5)
	WebUI.switchToWindowIndex(0)
//Log out from Receiver(TO)
	printStatemnet="Step "+(stepCount++)+": Logout the Dashboard from Receiver(To address)"
	verifyStatus=CustomKeywords.'application.LoginLogout.Logout'()
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
//Login to the Reply receiver Dashboard
	printStatemnet="Step "+(stepCount++)+": Login to the Reply receiver Dashboard"
	verifyStatus=CustomKeywords.'application.LoginLogout.Login'(url,userName,password)
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
//Navigate to the Reply receiver  Messages App
	printStatemnet="Step "+(stepCount++)+": Navigate to Reply receiver MessagesApp"
	 verifyStatus= CustomKeywords.'application.MessagesApp.MessagesAppNavigation'()
	 WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
//Verify reply receiver Inbox and Log out from messages app window
	printStatemnet="Step "+(stepCount++)+":Verify Messages In Reply receiver Inbox folder "
	WebUI.verifyElementPresent(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'("//td[text()='Re: "+mailSubject+"']"), 10, FailureHandling.OPTIONAL)
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
	WebUI.click(findTestObject('Application/Messages/MS_HomePage/MS_HP_LogoutMessageappWindow'),FailureHandling.STOP_ON_FAILURE)
	WebUI.delay(5)
	WebUI.switchToWindowIndex(0)
//Log out from Reply receiver
	printStatemnet="Step "+(stepCount++)+": Logout the Dashboard from Reply receiver"
	verifyStatus=CustomKeywords.'application.LoginLogout.Logout'()
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
	
scriptStatus=true
}catch(Exception e){
if(verifyStatus){
	printStatemnet=printStatemnet+" ->**** Exception occured***: "+e
	CustomKeywords.'reports.ReportFile.takeScreenshot'(test)
}
}finally{
		if(scriptStatus){
			test.log(LogStatus.PASS, TestCase);
			rep.endTest(test);
			rep.flush();
		}else{
			test.log(LogStatus.FAIL, TestCase);
			rep.endTest(test);
			rep.flush();
			WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test,false,printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
		}
		
}
