import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.junit.After

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import junit.framework.TestListener

import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


ExtentReports rep = CustomKeywords.'reports.ReportFile.getInstance'(reportFolder)
ExtentTest test
println test
scriptStatus=false
int stepCount=1
def TestCase=ENV+"- Add Non-Simple program in Various Configuration"
test = rep.startTest(TestCase)
println test
verifyStatus=false
def printStatement=""
try{
	//Login to the Dashboard
	printStatement="Step "+(stepCount++)+": Login into the Dashboard"
	verifyStatus=CustomKeywords.'application.LoginLogout.Login'(url,userID,password)
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatement), "Pass", false, FailureHandling.STOP_ON_FAILURE)
	
	def Consent ="//md-option/div[normalize-space(text())='"+ConsentStatusValue+"']"
	def Consenter="//*[normalize-space(text())='"+ConsenterValue+"']"

	

WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatement), "Pass", false, FailureHandling.STOP_ON_FAILURE)
//Navigate to Demographics App
printStatement="Step "+(stepCount++)+": Navigate to the Demographics App"
verifyStatus=CustomKeywords.'application.DemographicsApp.DemographcisAppNavigation'()
WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test,verifyStatus,printStatement), "Pass", false, FailureHandling.STOP_ON_FAILURE)
//Search patient with PatientLastName and Patient ID
printStatement="Step "+(stepCount++)+": Search Patient in the demographics App"
verifyStatus=CustomKeywords.'application.DemographicsApp.searchPatientInDemographcisApp'(patientID, patientLastName)
WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test,verifyStatus,printStatement), "Pass", false, FailureHandling.STOP_ON_FAILURE)
//Navigate to view patient Page
printStatement="Step "+(stepCount++)+": Navigate to the View Patient Page"
WebUI.click(findTestObject('Application/DemographicsApp/SearchPatientPage/DA_SPP_selectButton'), FailureHandling.STOP_ON_FAILURE)
verifyStatus=WebUI.verifyElementPresent(findTestObject('Application/DemographicsApp/ViewPatientPage/DA_VPP_viewPatientPageText'), 6, FailureHandling.OPTIONAL)
WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test,verifyStatus,printStatement), "Pass", false, FailureHandling.STOP_ON_FAILURE)
//Navigate to the Enrollment Wizard page
printStatement="Step "+(stepCount++)+": Navigate to the Enrollment Wizard page"
WebUI.click(findTestObject('Application/DemographicsApp/ViewPatientPage/DA_VPP_EnrollmentButton'), FailureHandling.STOP_ON_FAILURE)
verifyStatus=WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW'), 6, FailureHandling.OPTIONAL)
WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test,verifyStatus,printStatement), "Pass", false, FailureHandling.STOP_ON_FAILURE)
//Navigate to the Program Wizard page
WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW'), FailureHandling.STOP_ON_FAILURE)

printStatement="Step "+(stepCount++)+": Navigate the Program wizard"
verifyStatus=WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_AddIcon'), 4, FailureHandling.OPTIONAL)
WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test,verifyStatus,printStatement), "Pass", false, FailureHandling.STOP_ON_FAILURE)
//Delete Existing Pograms if Exist
if(WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_VerticalElipsisIcon'), 4, FailureHandling.OPTIONAL))
{
	printStatement="Step "+(stepCount++)+": Deleted all existing programs for the patients"
//	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test,true,printStatement), "Pass", false, FailureHandling.STOP_ON_FAILURE)
	def deleteProgramStatus=CustomKeywords.'application.ProgramWizard.deleteProgram'(10)
	if(deleteProgramStatus.equals("User deleted the the Programs")){
		verifyStatus=true
	}else{
	verifyStatus=false
	}
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test,verifyStatus,printStatement), "Pass", false, FailureHandling.STOP_ON_FAILURE)
}

//Add New Program - Add ICon

WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_AddIcon'), FailureHandling.STOP_ON_FAILURE)
WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_ParentNameDropdown'), FailureHandling.STOP_ON_FAILURE)
def parentNameValueXpath="//*[normalize-space(text())=\""+ParentProgram+"\"]"
	WebUI.click(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'(parentNameValueXpath), FailureHandling.STOP_ON_FAILURE)
	/*
	 //Parent-ConsentField
	 	 
	if(PPConsent.equals("Y"))
	{
	   
		WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_ConsentSelectStatusDropdown'), FailureHandling.STOP_ON_FAILURE)
		WebUI.click(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'(Consent), FailureHandling.STOP_ON_FAILURE)
		//Consenter dropdown field
        if(PPConsenter.equals("Y"))
		{
			WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_ConsentPopupConsenterDropdown'), FailureHandling.STOP_ON_FAILURE)
		    
			WebUI.click(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'(Consenter), FailureHandling.STOP_ON_FAILURE)
		
		//Minor Informationsharing Enable
				if(PPMinor.equals("Y"))
				{
					WebUI.click(findTestObject('Application/BetaProgram/ConsentPopupMinorBar'), FailureHandling.STOP_ON_FAILURE)
					}
		}
				
		
		WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_ConsentSaveFinishButton'), FailureHandling.STOP_ON_FAILURE)
	}*/

//Child Program Selection From ProgramName DropDown
	WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_PogramNameDropdown'), FailureHandling.STOP_ON_FAILURE)
	//Select the ProgramName
	WebUI.delay(2)
	def ProgramName="//md-option[contains(@ng-repeat,'SubProgram')]/div[normalize-space(text())=\""+ChildProgram+"\"]"
	//md-option/div[normalize-space(text()) = 'Maternal and Infant Home Visiting']
	WebUI.click(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'(ProgramName), FailureHandling.STOP_ON_FAILURE)
	
		//Child-ConsentField
		if(PConsent.equals("Y"))
		{
		   
			WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_ConsentSelectStatusDropdown'), FailureHandling.STOP_ON_FAILURE)
			//ConsentValue
			WebUI.delay(2)
			WebUI.click(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'(Consent), FailureHandling.STOP_ON_FAILURE)
			//Consenter dropdown field
			if(PConsenter.equals("Y"))
			{
				WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_ConsentPopupConsenterDropdown'), FailureHandling.STOP_ON_FAILURE)
				//ConsenterValue
				WebUI.click(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'(Consenter), FailureHandling.STOP_ON_FAILURE)
			//MinorInformationsharingEnable
					if(PMinor.equals("Y"))
					{
						WebUI.click(findTestObject('Application/BetaProgram/ConsentPopupMinorBar'), FailureHandling.STOP_ON_FAILURE)
						}
					}
			
			WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_ConsentSaveFinishButton'), FailureHandling.STOP_ON_FAILURE)
		}
	

//Program Status
WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_StatusDropdown'), FailureHandling.STOP_ON_FAILURE)
WebUI.delay(2)
//verify Program status is Inactive
def programStatus="//*[normalize-space(text())='"+Status+"']"

	WebUI.click(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'(programStatus), FailureHandling.STOP_ON_FAILURE) 
		if(EndReason.equals("Y"))
		{
			WebUI.click(findTestObject('Application/BetaProgram/ProgramEndReasonDropdown'), FailureHandling.STOP_ON_FAILURE)
			def ProgramEndReason="//*[normalize-space(text())='"+EndReasonValue+"']"
			WebUI.click(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'(ProgramEndReason), FailureHandling.STOP_ON_FAILURE)
		}		

//Save The Program
WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_ProgramSaveButton'), FailureHandling.STOP_ON_FAILURE)
verifyStatus=WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_ProgramSavedVerification'), 6, FailureHandling.OPTIONAL)

////Delete the Program
//printStatement="Step "+(stepCount++)+": Delete the newly AddedProgram"
//WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test,true,printStatement), "Pass", false, FailureHandling.OPTIONAL)
//def deleteProgramStatus=CustomKeywords.'application.ProgramWizard.deleteProgram'(10)
//println "+*****************"+deleteProgramStatus
//if(deleteProgramStatus.equals("User deleted the the Programs")){
//	verifyStatus=true
//}else{
//verifyStatus=false
//}
//WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test,verifyStatus,printStatement), "Pass", false, FailureHandling.STOP_ON_FAILURE)


//Close Wizard

WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/EACW_WizardCloseIcon'), FailureHandling.STOP_ON_FAILURE)
//WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test,verifyStatus,printStatement), "Pass", false, FailureHandling.STOP_ON_FAILURE)
//Logout the Dashboard
printStatement="Step "+(stepCount++)+": Logout from Dashboard"
	verifyStatus=CustomKeywords.'application.LoginLogout.Logout'()
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatement), "Pass", false, FailureHandling.STOP_ON_FAILURE)
	scriptStatus = true
}catch (Exception e) {
    if (verifyStatus) {
        printStatement = ((printStatement + ' ->**** Exception occured***: ') + e)

        CustomKeywords.'reports.ReportFile.takeScreenshot'(test)
    }
} 
finally { 
    if (scriptStatus) {
		println('Pass')
        test.log(LogStatus.PASS, TestCase)
        rep.endTest(test)
        rep.flush()
		WebUI.closeBrowser()
    } else {
	 println('Fail')
	
        test.log(LogStatus.FAIL, TestCase)
        rep.endTest(test)
        rep.flush()
		WebUI.closeBrowser()
        WebUI.verifyMatch("Config Verification", 'Config Verification Failed', false, FailureHandling.STOP_ON_FAILURE)
    }
}