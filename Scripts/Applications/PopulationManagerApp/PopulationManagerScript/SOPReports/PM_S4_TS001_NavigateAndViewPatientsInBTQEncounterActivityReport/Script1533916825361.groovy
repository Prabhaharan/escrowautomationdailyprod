import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.apache.commons.lang.StringUtils;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



//def fromDate=TestDataFactory.findTestData("PopulationManager/CommonParameter").getValue("fromDate", 1)
//FromDate-Test Data

ExtentReports rep = CustomKeywords.'reports.ReportFile.getInstance'(reportFolder)
ExtentTest test
println test
scriptStatus=false
int stepCount=1
def TestCase=ENV+"_PM_S4_TS001_NavigateAndViewPatientsInBTQEncounterActivityReport"
test = rep.startTest(TestCase)
println test
verifyStatus=false
def printStatemnet=""

try{

    //Login to the Dashboard
	printStatement="Step "+(stepCount++)+": Login into the Dashboard"
	verifyStatus=CustomKeywords.'application.LoginLogout.Login'(url,userName,password)
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatement), "Pass", false, FailureHandling.STOP_ON_FAILURE)
    WebUI.delay(3)
	//Click and Navigate to Population Manager App
	printStatement="Step "+(stepCount++)+": Click and Navigate to Population Manager App"
	verifyStatus=CustomKeywords.'application.PopulationManagerIcon.PopulationManagerAppNavigation'()
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatement), "Pass", false, FailureHandling.STOP_ON_FAILURE)
	WebUI.delay(3)
    //Navigate to SOP folder
    printStatement="Step "+(stepCount++)+": Navigate to SOP folder"
	verifyStatus=CustomKeywords.'application.PopulationManagerIcon.StandardOperationReportNavigation'()
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatement), "Pass", false, FailureHandling.STOP_ON_FAILURE)
    WebUI.delay(3)
	// Navigate to Alerts Report
    WebUI.doubleClick(findTestObject('Application/PopulationManagerApp/Pop_Mgr/Home/BTQEncounterActivityReport'), FailureHandling.STOP_ON_FAILURE)
    printStatemnet="Step "+(stepCount++)+": Navigate to BTQEncounterActivityReport"
    verifyStatus=WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30, FailureHandling.STOP_ON_FAILURE)
    WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
    WebUI.switchToFrame(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_0'), 30)
    WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/FromExtractDate'),50, FailureHandling.STOP_ON_FAILURE)
    WebUI.delay(3)
	// Select Parameter Values
	
printStatemnet="Step "+(stepCount++)+": Select Parameter Values"
verifyStatus=WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/FromExtractDate'), 30, FailureHandling.STOP_ON_FAILURE)
WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
TestObject parentProgramSelectXpath = findTestObject('Object Repository/Base/commanAlertXpath')
parentProgramSelectXpath.findProperty('xpath').setValue("//*[text()='Parent Program']/following::select[1]/option[text()='"+ParentProgram+"']")
WebUI.scrollToElement(parentProgramSelectXpath, 10, FailureHandling.STOP_ON_FAILURE)
WebUI.doubleClick(parentProgramSelectXpath)
WebUI.delay(3)
TestObject programNameSelectXpath = findTestObject('Object Repository/Base/commanAlertXpath')
programNameSelectXpath.findProperty('xpath').setValue("//*[text()='Program Name']/following::select[1]/option[text()='"+ProgramName+"']")
WebUI.scrollToElement(programNameSelectXpath, 10, FailureHandling.STOP_ON_FAILURE)
WebUI.doubleClick(programNameSelectXpath)
WebUI.delay(3)
TestObject programStatusSelectXpath = findTestObject('Object Repository/Base/commanAlertXpath')
programStatusSelectXpath.findProperty('xpath').setValue("//*[text()='Program Status']/following::select[1]/option[text()='"+ProgramStatus+"']")
WebUI.scrollToElement(programStatusSelectXpath, 10, FailureHandling.STOP_ON_FAILURE)
WebUI.doubleClick(programStatusSelectXpath)
WebUI.delay(3)
WebUI.setText(findTestObject('Object Repository/Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/EncounterStartDate'), EncounterStartDate)

WebUI.setText(findTestObject('Object Repository/Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/EncounterEndDate'), EncounterEndDate)
WebUI.delay(3)
WebUI.setText(findTestObject('Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/PatientID'), PatientID)
TestObject organizationSelectXpath = findTestObject('Base/commanXpath')
organizationSelectXpath.findProperty('xpath').setValue("//*[text()='Organization']/following::select[1]/option[text()='"+Organization+"']")
WebUI.delay(5)
//WebUI.scrollToElement(organizationSelectXpath, 5, FailureHandling.STOP_ON_FAILURE)
WebUI.doubleClick(organizationSelectXpath)
//WebUI.doubleClick(findTestObject('Object Repository/Applications/PopulationManagerApp/Pop_Mgr-Sopvalidation/Organization_Click'))
WebUI.delay(3)
WebUI.setText(findTestObject('Object Repository/Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/FromExtractDate'), FromExtractDate)
WebUI.delay(3)
WebUI.setText(findTestObject('Object Repository/Application/PopulationManagerApp/Pop_Mgr-Sopvalidation/ToExtractDate '), ToExtractDate)


//Click on the View Reports button
WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/View Report '), 30, FailureHandling.STOP_ON_FAILURE)
WebUI.doubleClick(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/View Report '))
WebUI.delay(3)
WebUI.switchToFrame(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Frame_reportContent'), 30)
//WebUI.verifyElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Patient ID'), 120, FailureHandling.STOP_ON_FAILURE)
//printStatemnet="Step "+(stepCount++)+": Verify patientId details display in the view report"
String a = WebUI.getText(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/ProviderID'), FailureHandling.STOP_ON_FAILURE)
boolean patientIDnumericValue = CustomKeywords.'application.PopulationManagerIcon.isNumericValue'(a)
if(patientIDnumericValue){
WebUI.verifyMatch("PatientID is dispalyed in the SOP Report Page", "PatientID is dispalyed in the SOP Report Page", true)
printStatemnet="Step "+(stepCount++)+": Verified patientId details displayed in the view report"
}else{
WebUI.verifyMatch("PatientID is dispalyed in the SOP Report Page", "PatientID is not dispalyed in the SOP Report Page", true)
printStatemnet="Step "+(stepCount++)+": Verified patientId details are not displayed in the view report"
}
WebUI.closeWindowIndex(1)
WebUI.switchToDefaultContent()
//Logout the Dashboard
printStatement="Step "+(stepCount++)+": Logout from Dashboard"
	verifyStatus=CustomKeywords.'application.LoginLogout.Logout'()
	WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test, verifyStatus, printStatement), "Pass", false, FailureHandling.STOP_ON_FAILURE)
scriptStatus=true
}catch(Exception e){
if(verifyStatus){
printStatemnet=printStatemnet+" ->**** Exception occured***: "+e
CustomKeywords.'reports.ReportFile.takeScreenshot'(test)
}
}finally{
	if(scriptStatus){
		test.log(LogStatus.PASS, TestCase);
		rep.endTest(test);
		rep.flush();
	}else{
		test.log(LogStatus.FAIL, TestCase);
		rep.endTest(test);
		rep.flush();
		WebUI.verifyMatch(CustomKeywords.'reports.ReportFile.print'(test,false,printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
	}
	
}

