<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>reportConfig</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-08-14T09:58:54</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>90566537-d898-4d4d-a0bb-fa3669f62c7a</testSuiteGuid>
   <testCaseLink>
      <guid>e6f69665-9eaa-43ee-bcb7-c174c48e0208</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Base/ReportConfig</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>0382d2ce-25ca-4830-b15e-baf7b53550fd</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Messages/Messages_DataSheet</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>0382d2ce-25ca-4830-b15e-baf7b53550fd</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>reportFolder</value>
         <variableId>d9d3f4ea-964f-419c-9784-87b132cf8f17</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
